package Youtube.com.youtube;


import org.apache.hc.client5.http.cookie.CommonCookieAttributeHandler;
import org.apache.tools.ant.taskdefs.WaitFor;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.codeborne.selenide.commands.Click;

import io.github.bonigarcia.wdm.WebDriverManager;



public class ApptestTest {
	
	
	public JavascriptExecutor jsExecutor;
	public WebDriver driver;
	Commanmethod commanmethod =new Commanmethod();
	webDriverUtils open_Application = new webDriverUtils();
	//Actions actions = new Actions(driver);
	//	actions.moveToElement(element);
  @BeforeClass
  public void initialSetup() {
	  
	  try {
		  String gmailid=commanmethod.readPropertiefile().getProperty("username"); 
		  String HubUrl= commanmethod.readPropertiefile().getProperty("hubUrl");
		  String server=commanmethod.readPropertiefile().getProperty("server");
		  String browser=commanmethod.readPropertiefile().getProperty("browser");
		  String url_Val= commanmethod.readPropertiefile().getProperty("applicationUrl");
		  System.out.println("Application URL is  [" + url_Val +" ]");
		  System.out.println("^^^^^^^^ Before Test Execution Started  ^^^^^^^");
		  System.out.println("Execution Enviroment is [ "+server+" ] and URL is [ "+ url_Val + "] and Browser is [ " + browser +" ]");
		  driver = commanmethod.openApplication_login(driver, browser, server, HubUrl);
		  Thread.sleep(2000);
		  System.out.println("Driver Initiated"+driver.getWindowHandles());
		  boolean loginstatus= commanmethod.openbrowser(driver, url_Val);
		  System.out.println("^^^^^^^^  Test 1 Execution END  ^^^^^^^");
		  if (!loginstatus )
			  throw new SkipException("Not Able to launch the Browser and URL ");
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	  
  }
  
  @AfterClass
  public void logou() {
	  System.out.println("$$$$$$$$$$   Closining Application Done Successfully $$$$$$$");
	  driver.quit();
	  
  }
  
 
  
  
/* @Test(priority=1)
public void test() throws Exception {
	  String HubUrl= commanmethod.readPropertiefile().getProperty("hubUrl");
	  String server=commanmethod.readPropertiefile().getProperty("server");
	  String browser=commanmethod.readPropertiefile().getProperty("browser");
	  String url_Val= commanmethod.readPropertiefile().getProperty("applicationUrl");
	  System.out.println("Application URL is  [" + url_Val +" ]");
	  try {
		  System.out.println("^^^^^^^^  Test 1 Execution Started  ^^^^^^^");
		  System.out.println("Execution Enviroment is [ "+server+" ] and URL is [ "+ url_Val + "] and Browser is [ " + browser +" ]");
		  driver = commanmethod.openApplication_login(driver, browser, server, HubUrl);
		  Thread.sleep(2000);
		  System.out.println("Driver Initiated"+driver.getWindowHandles());
		  boolean loginstatus= commanmethod.openbrowser(driver, url_Val);
		  System.out.println("^^^^^^^^  Test 1 Execution END  ^^^^^^^");
		  if (!loginstatus )
			  throw new SkipException("Not Able to launch the Browser and URL ");
	} catch (Exception e) {
		System.out.println(e.getMessage());
		// TODO: handle exception
	}
	
	
		
	
} */
  
  @Test(priority =1)
  public void Search_with_card_detailTest() {
	  
	  try {
		  System.out.println("^^^^^^^^  Test 1 Execution Started  ^^^^^^^");
		  String videoHastag = commanmethod.readPropertiefile().getProperty("videoHashtag1");
		  String videoHastag2 = commanmethod.readPropertiefile().getProperty("videoHashtag2");
		  String videoHastag3 = commanmethod.readPropertiefile().getProperty("videoHashtag3");
		  String videoHastag4 = commanmethod.readPropertiefile().getProperty("videoHashtag4");
		  String videoHastag5 = commanmethod.readPropertiefile().getProperty("videoHashtag5");
		  String videoTitel =commanmethod.readPropertiefile().getProperty("videoTitle");
		  System.out.println("*****   "+videoTitel+ " ********");
		  System.out.println("*****   "+videoHastag+ " ********");
		  System.out.println("*****   "+videoHastag2+ " ********");
		  System.out.println("*****   "+videoHastag3+ " ********");
		  System.out.println("*****   "+videoHastag4+ " ********");
		  System.out.println("*****   "+videoHastag5+ " ********");
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played 1  ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag2, videoTitel);
		  System.out.println("*****   Video Played Successfully 2 Time ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag3, videoTitel);
		  System.out.println("*****   Video Played Successfully With 3 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag4, videoTitel);
		  System.out.println("*****   Video Played Successfully With 4 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played Successfully With 5 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag5, videoTitel);
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played 6 Successfully ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag2, videoTitel);
		  System.out.println("*****   Video Played Successfully 7 Time ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag3, videoTitel);
		  System.out.println("*****   Video Played Successfully With 8 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played Successfully With 9 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played Successfully With 10 Tag ********");
		  commanmethod.openTab(driver);
         
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played Successfully 11 ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag2, videoTitel);
		  System.out.println("*****   Video Played Successfully 12 Time ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag3, videoTitel);
		  System.out.println("*****   Video Played Successfully With 13 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag4, videoTitel);
		  System.out.println("*****   Video Played Successfully With 14 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag5, videoTitel);
		  System.out.println("*****   Video Played Successfully With 15 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played Successfully 16 ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag2, videoTitel);
		  System.out.println("*****   Video Played Successfully 17 Time ********");
		  commanmethod.openTab(driver);
		  
		  commanmethod.StopThread(driver, jsExecutor, "(//a[@class='ytp-ce-covering-overlay'])[2]" ,12);
          commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
          commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
		 
		 
		 
		  
		  commanmethod.runthevideo(driver, videoHastag3, videoTitel);
		  System.out.println("*****   Video Played Successfully With 18 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag4, videoTitel);
		  System.out.println("*****   Video Played Successfully With 19 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag5, videoTitel);
		  System.out.println("*****   Video Played Successfully With 20 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played Successfully 21 ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag2, videoTitel);
		  System.out.println("*****   Video Played Successfully 22 Time ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag3, videoTitel);
		  System.out.println("*****   Video Played Successfully With 23 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag4, videoTitel);
		  System.out.println("*****   Video Played Successfully With 24 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag5, videoTitel);
		  System.out.println("*****   Video Played Successfully With 25 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  System.out.println("*****   Video Played 26  Successfully ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag2, videoTitel);
		  System.out.println("*****   Video Played Successfully 27 Time ********");
		  
		  commanmethod.StopThread(driver, jsExecutor, "(//a[@class='ytp-ce-covering-overlay'])[2]" ,12);
          commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
          commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
          commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
          commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
          commanmethod.Switchtoparentwindow(driver);
		  commanmethod.Switchtoparentwindow(driver);
		  
	
		  
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag3, videoTitel);
		  System.out.println("*****   Video Played Successfully With 28 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag4, videoTitel);
		  System.out.println("*****   Video Played Successfully With 29 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag5, videoTitel);
		  System.out.println("*****   Video Played Successfully With 30 Tag ********");
		  commanmethod.openTab(driver);
		  commanmethod.runthevideo(driver, videoHastag, videoTitel);
		  commanmethod.StopThread(driver, jsExecutor, "(//a[@class='ytp-ce-covering-overlay'])[2]" ,12);
		  System.out.println("^^^^^^^^  Test 2 Execution END  ^^^^^^^");
		 	
	} catch (Exception e) {
		System.out.print(e);
		driver.quit();
		// TODO: handle exception
	}

  }
  
 


 
}
