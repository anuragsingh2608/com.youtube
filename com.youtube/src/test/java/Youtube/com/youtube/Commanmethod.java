package Youtube.com.youtube;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.util.Trie;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;




public class Commanmethod {
	public static Properties properties;
	public static String propfilename="config.properties";
	webDriverUtils openApplication = new webDriverUtils();
	public JavascriptExecutor jsExecutor;
	public static Properties readPropertiefile() {
		try {
			properties = new Properties();
			String normalized_conf=Paths.get(propfilename).normalize().toString();
			File file = new File("Applicationfile",propfilename);
			FileInputStream fs = new FileInputStream(file.getCanonicalPath());
			properties.load(fs);
		} catch (Exception e) {
			System.out.println("Unable to Load Properties file");
			e.printStackTrace();
			// TODO: handle exception
		}
		return properties;
	}
	
	public String decodepassword(String pass) {
		String password =readPropertiefile().getProperty(pass);
		byte[] decpass = Base64.getDecoder().decode(password);
		return decpass.toString();
	}
	
	
	public boolean openbrowser(WebDriver driver, String url_val) {
		try {
			//url_val= readPropertiefile().getProperty("applicationUrl");
			driver.get(url_val);
			driver.manage().window().maximize();
			waitforpageload(driver);
			String title=driver.getTitle();
			if (title.contains("YouTube")){
				System.out.println("Alrady Load the Application");
				return true;
			}else {
				boolean LoginStatus =loginYoutube( driver, url_val );
				return LoginStatus;
			}
		
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
			// TODO: handle exception
		}
		
	}
	
	
	
	
	
	private boolean loginYoutube(WebDriver driver, String url_val) {
		
		try {
			String Title =driver.getTitle();
			if (Title.toLowerCase().contains("youtube")) {
				System.out.println("Login Success to you tube Application");
				return true;
				
			}else {
				driver.get(readPropertiefile().getProperty("url_val"));
				driver.manage().window().maximize();
				waitforpageload(driver);
				System.out.println("Login Success to you tube Application");
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
			// TODO: handle exception
		}
		
		
	}
	
	
	public WebDriver openApplication_login(WebDriver driver, String browser_type, String Local_grid, String huburl_val) throws Exception {
		driver = openApplication.getBrowser(browser_type, Local_grid, huburl_val);
		return driver;
	}




	
	
public boolean click(WebDriver driver, String Xpath_value) throws InterruptedException {
		
		try {
			
			if (call_ExplicitWait(driver, Xpath_value)==true ) {
				driver.findElement(By.xpath(Xpath_value)).click();
				waitforpageload(driver);
				return true;
			}else {
				System.out.println("Elemnt is not available in UI so Unable to click on it");
				return false;
			}
			
		} catch (Exception e) {
			System.out.println(e);
			return false;
			// TODO: handle exception
		}
		
	}
	
	
public boolean waitforelemntpresent(WebDriver driver, String Xpath_value) throws InterruptedException {
		
		boolean obj_visible= false;
		System.out.println("Control Move to Explicit Wait");
		Thread.sleep(2000);
		WebDriverWait wait =new WebDriverWait(driver , Duration.ofSeconds(240));
		do {
		WebElement element = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath_value)));
			obj_visible=element.isEnabled();		
		} while (!obj_visible);
		System.out.println("Element Display "+ obj_visible );
		return obj_visible;
		
		
	}


public void waitforDescriptiontobevisible(WebDriver driver, JavascriptExecutor jsExecutor,  String Xpath_value) throws InterruptedException {
	
	try {
		System.out.println("********** Control Move to waitforDescriptiontobevisible method **********");
		WebDriverWait wait =new WebDriverWait(driver , Duration.ofSeconds(10));
		jsExecutor = (JavascriptExecutor) driver;
		boolean des_visible= false;
		boolean pageload =((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
		System.out.println("********** Page Load Status is  [" +pageload +"]********** ");
		if (!driver.findElement(By.xpath(Xpath_value)).isEnabled()==true) {
			WebElement titlevalu = driver.findElement(By.xpath(Xpath_value));
			System.out.println("*************** Title Present in Screen **************");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({block: 'center', inline: 'nearest'});", titlevalu);
			System.out.println("**********   scroll Title in to Center ********** ");
			Thread.sleep(2000);
			jsExecutor.executeScript("arguments[0].style.border='2px solid red'", titlevalu); 
			Thread.sleep(2000);
			jsExecutor.executeScript("arguments[0].style.background='yellow'", titlevalu); 
			titlevalu.click();
			
		}else {
			System.out.println("**********   Title is not present in Screen ********** ");
			jsExecutor.executeScript("window.scrollBy(0,5000)");
			Thread.sleep(1000);
			jsExecutor.executeScript("window.scrollBy(0,5000)");
			jsExecutor.executeScript("window.scrollBy(0,5000)");
			jsExecutor.executeScript("window.scrollBy(0,5000)");
			jsExecutor.executeScript("window.scrollBy(0,3000)");
			Thread.sleep(1000);
			jsExecutor.executeScript("window.scrollBy(0,2000)");
			Thread.sleep(1000);
			System.out.println("Scroll Down Done");
		}
		  
       
		
	} catch ( SkipException e) {
		System.out.println(e);
		// TODO: handle exception
	}
}


public void playVideo(WebDriver driver,  JavascriptExecutor jsExecutor,  String Title)throws Exception{
    try {
    	String Xpath_value = "//a[@title= '"+Title+"']";
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		waitforpageload(driver);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		waitforpageload(driver);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		waitforpageload(driver);
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,5000)");
		waitforpageload(driver);
		waitforpageload(driver);
		Thread.sleep(5000);
		jsExecutor.executeScript("window.scrollBy(0,-75000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,-75000)");
		jsExecutor.executeScript("window.scrollBy(0,-75000)");
		Thread.sleep(500);
		jsExecutor.executeScript("window.scrollBy(0,-75000)");
		Thread.sleep(5000);
    	waitforpageload(driver);
        List<WebElement> links = driver.findElements(By.xpath("//a[@id= 'video-title']"));
        int linkcount = links.size(); 
         System.out.println("Link Present on the page is "  +links.size()); 
         for (WebElement myElement : links){
         String titleName = myElement.getText(); 
        // System.out.println("Here is the List of link present in page " +link);
         System.out.println(titleName);   
        if (titleName.equalsIgnoreCase(Title)) {
        	WebElement titlevalu = driver.findElement(By.xpath(Xpath_value));
        	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({block: 'center', inline: 'nearest'});", titlevalu);
			System.out.println("**********   scroll Title in to Center ********** ");
			Thread.sleep(2000);
			jsExecutor.executeScript("arguments[0].style.border='2px solid red'", titlevalu); 
			Thread.sleep(2000);
			jsExecutor.executeScript("arguments[0].style.background='yellow'", titlevalu); 
            myElement.click();
            System.out.println("**********Video Play Successufully ***********");
            break;
            }
            //Thread.sleep(5000);
          } 
        }catch (Exception e){
            System.out.println("error "+e);
        }
    }
		
	
	
	
	
	


public void ifpresent_thenclick(WebDriver driver, JavascriptExecutor jsExecutor,  String Xpath_value) throws InterruptedException {
	try {
		WebDriverWait wait =new WebDriverWait(driver , Duration.ofSeconds(60));
		boolean des_visible= false;
		System.out.println("Control Move to Check the Elemnt is presnt");
			des_visible=driver.findElement(By.xpath(Xpath_value)).isEnabled();	
			
			if (des_visible) {
				driver.findElement(By.xpath(Xpath_value)).click();
					//obj_Button.click();
					System.out.println("*******  Button Prsent in UI and Click Successfully***********");
			}else {
				
				wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath_value)));
				driver.findElement(By.xpath(Xpath_value)).click();
				System.out.println("*******  Button Prsent in UI and Click Successfully***********");
			}
		
	} catch (Exception e) {
	}
		
}

public boolean call_ExplicitWait(WebDriver driver, String Xpath_value) throws InterruptedException {
	
	boolean obj_visible= false;
	System.out.println("Control Move to Explicit Wait");
	Thread.sleep(2000);
	WebElement element;
	WebDriverWait wait =new WebDriverWait(driver , Duration.ofSeconds(60));
	try {
		if ((Xpath_value.contains("//button")) || (Xpath_value.contains("//div")) ||  (Xpath_value.contains("//a")) || (Xpath_value.contains("//input")))  {
			System.out.println("Waiting for elemnt to be clickable");
			element = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(By.xpath(Xpath_value)));
			obj_visible=element.isEnabled();
		}else {
			System.out.println("Waiting for elemnt to be clickable");
			element = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath_value)));
			obj_visible=element.isEnabled();
		}
		System.out.println("Explicit wait Finish");
		return obj_visible;
		
	} catch (Exception e) {
		System.out.println(e);
		return obj_visible;
		// TODO: handle exception
	}
	
}


	public boolean clickonFilterbutton(WebDriver driver, JavascriptExecutor jsExecutor,  String Xpath_value) throws InterruptedException {
		
		try {
			WebDriverWait wait =new WebDriverWait(driver , Duration.ofSeconds(5));
			boolean des_visible= false;
			System.out.println("Control Move to filter the video ");
			WebElement visible1;
			des_visible = driver.findElement(By.xpath(Xpath_value)).isDisplayed();
			if (des_visible) {
				visible1 = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath_value)));
				jsExecutor.executeScript("arguments[0].style.background='yellow'", visible1);
				//visible1.click();
				jsExecutor.executeScript("arguments[0].click();", visible1);
				Thread.sleep(5000);
				driver.findElement(By.xpath("//div[contains(@title,'Today') and @id='label']")).click();
				//driver.findElement(By.xpath("//div[contains(@title,'This week') and @id='label']")).click();
				//driver.findElement(By.xpath("//div[contains(@title,'Search for This month') and @id='label']")).click();
				System.out.println("Short Done");
			}else {
				visible1 = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath_value)));
				driver.findElement(By.xpath(Xpath_value)).click();
				
			}
		} catch (Exception Message) {
		} 
		return true;
		}
	


	
	
public boolean witforvideotobeend(WebDriver driver, JavascriptExecutor jsExecutor,  String Xpath_value) throws InterruptedException {
	
	boolean des_visible= false;
	System.out.println("Control Move to Explicit Wait");
	for (int i = 0; i < 500; ) {
		des_visible=driver.findElement(By.xpath(Xpath_value)).isEnabled();	
		if (des_visible) {
			WebElement obj_Button= driver.findElement(By.xpath(Xpath_value));
				jsExecutor.executeScript("arguments[0].style.background='yellow'", obj_Button); 
				obj_Button.click();
				System.out.println("Video End Successfully Closeing the App");
				driver.close();
			break;
		}else {
			
			i++;
			Thread.sleep(2000);
		}
		
		
	}
	return des_visible;
	

}


    public boolean waitforpageload(WebDriver driver) {
        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
    }
    
    
    public void StopThread(WebDriver driver, JavascriptExecutor jsExecutor,  String Xpath_value , int timetoStop) throws InterruptedException {
    	try {
    		WebDriverWait wait =new WebDriverWait(driver , Duration.ofMinutes(timetoStop));
    		Thread.sleep(20000);
    		boolean des_visible= false;
    		System.out.println("Execution Started to play the video for 12 min");
    			des_visible=driver.findElement(By.xpath(Xpath_value)).isDisplayed();	
    			if (des_visible) {
    				Thread.sleep(20000);
    				WebElement obj_Button= driver.findElement(By.xpath(Xpath_value));
    				jsExecutor.executeScript("arguments[0].style.background='yellow'", obj_Button); 
    				obj_Button.click();
    				System.out.println("*********************** Succesuffly Clicked on Card Succefully*****************");
    				Thread.sleep(20000);
    			}else {
    				wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath_value)));
    				Thread.sleep(2000);
    				driver.quit();
    			}
    	
    		
    		
    	} catch (SkipException e) {
    		System.out.println(e);
    		
    		

    		// TODO: handle exception
    	}



    }
    
    
    public boolean runthevideo(WebDriver driver, String videohashtag , String titleText) throws Exception {
    	try {
    		waitforpageload(driver);
    		jsExecutor = (JavascriptExecutor) driver;
    		driver.findElement(By.xpath("//input[@id='search']")).sendKeys(videohashtag);
    		Thread.sleep(3000);
    		driver.findElement(By.xpath("(//button[@id='search-icon-legacy']/*[@class='style-scope ytd-searchbox'])[1]")).click();
    		Thread.sleep(2000);
    		driver.findElement(By.xpath("(//button[@id='search-icon-legacy']/*[@class='style-scope ytd-searchbox'])[1]")).click();
    		waitforpageload(driver);
    		Thread.sleep(5000);
			clickonFilterbutton(driver,jsExecutor, "//*[contains(text(),'Filters')]");
			System.out.println("**********  Content is short by One Month  **********");
			waitforpageload(driver);
			Thread.sleep(4000);
			playVideo(driver, jsExecutor, titleText);
			//waitforDescriptiontobevisible(driver, jsExecutor, "//a[@title= 'Best Fog Light for Car - Ecosports Fog light Installation(Vaishnu) - Must Watch']");
	        driver.navigate().refresh();
			waitforpageload(driver);
			System.out.println("######   Refresh Done   ######");
			ifpresent_thenclick(driver, jsExecutor,"//span[contains(text(),'Skip trial')]");
			System.out.println("***********   Clicked on Skip Trial Button  *********");
			ifpresent_thenclick(driver, jsExecutor, "//div[contains(@id,'skip-button')]//child::span[contains(@class,'button-container')]");
		    System.out.println("**********   Clicked on Skip Ads Button  ********");
		    Thread.sleep(1000);
			
		    ifpresent_thenclick(driver, jsExecutor, "//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		   // commanmethod.ifpresent_thenclick(driver, jsExecutor,"//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		    System.out.println("********Disable  auto play Option  *******");
		    ifpresent_thenclick(driver, jsExecutor,"//button[contains(@title,'Mute') and contains(@class,'ytp-mute-button')]");
		    System.out.println("***** Mute the Video ******** ");
		    Thread.sleep(5000);
    		return true;
		} catch ( SkipException e) {
			e.printStackTrace();
			return false;
			// TODO: handle exception
		}
    	
    }
    
    public void SeleniumTakeScreenshot(WebDriver driver) throws IOException {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("./image.png"));
        }
    
    
    
    public boolean openWindow(RemoteWebDriver driver) {
    	try {
    		driver.switchTo().newWindow(WindowType.WINDOW);
        	driver.get("https://youtube.com");
        	if (driver.getTitle().equalsIgnoreCase("youtube")) {
        		System.out.println("$$$$$$   Switch To new Window Done    $$$$$$");
				
			}else {
				driver.get("https://youtube.com");
				System.out.println("$$$$$$   Switch To new Window Done    $$$$$$");
			}
    		return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			// TODO: handle exception
		}
   	
    }
    
    public boolean openTab(WebDriver driver) {
    	try {
    		driver.switchTo().newWindow(WindowType.WINDOW);
        	driver.get("https://youtube.com");
        	if (driver.getTitle().equalsIgnoreCase("youtube")) {
        		System.out.println("$$$$$$   Switch To new Tab Done    $$$$$$");
				
			}else {
				driver.get("https://youtube.com");
				System.out.println("$$$$$$   Switch To new Tab Done    $$$$$$");
			}
    		return true;
			
    		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
   	
    }
    
    public void Switchtoparentwindow(WebDriver driver) {
    	Set<String> handels = driver.getWindowHandles();
    	List<String > windowlist =new ArrayList<String>(handels);
    	String parentwindow =windowlist.get(0);
    	System.out.println("***  PARENT Window ID [ ***"+windowlist.get(0)+ "]");
    	System.out.println("***  CHILD Window ID [ ***"+windowlist.get(1)+ "]");
    	driver.switchTo().defaultContent();
    	driver.switchTo().window(parentwindow);
    	System.out.println("Window Closed Successfully [ " +parentwindow +"]" );
    	driver.close();
    	
    }



}
