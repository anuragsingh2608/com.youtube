package Youtube.com.youtube;

import java.net.URL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class webDriverUtils {
	public WebDriver driver;
	
	public WebDriver getBrowser(String browsertype, String server, String HubURL) throws Exception {
	
		if(browsertype.equalsIgnoreCase("FireFox")) {
			
			if(server.equalsIgnoreCase("Remote")&& !HubURL.isEmpty()) {
				FirefoxOptions options = new FirefoxOptions();
				driver = new RemoteWebDriver(new URL(HubURL), options);
				
			}else {
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				System.out.println("Browser select to FireFox");
			}
		} else if (browsertype.equalsIgnoreCase("Chrome")) {
			if(server.equalsIgnoreCase("Remote")&& !HubURL.isEmpty()) {
				ChromeOptions options = new ChromeOptions();
				driver = new RemoteWebDriver(new URL(HubURL), options);
				
			}else {
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				System.out.println("Browser select to CHROME");
			}
			
		} else if (browsertype.equalsIgnoreCase("HD")) {
			    WebDriverManager.chromedriver().setup();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
				options.addArguments("disable-infobars");
				options.addArguments("headless");
				options.addArguments("window-size=1920,1200");
				driver = new ChromeDriver(options);
				System.out.println("Browser select as HEADLESS Browser");
				return driver;
				
		}else {
			throw new Exception("Browser is not Correct [ " + browsertype + "]");
		}
		
		System.out.println("Driver Instance Created ");
		return driver;	
	}
	
	
	
	
	
}//Class


	
	
	
