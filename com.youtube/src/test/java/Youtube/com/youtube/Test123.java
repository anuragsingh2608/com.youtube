package Youtube.com.youtube;


import org.apache.tools.ant.taskdefs.WaitFor;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.codeborne.selenide.commands.Click;

import io.github.bonigarcia.wdm.WebDriverManager;



public class Test123 {
	
	
	public JavascriptExecutor jsExecutor;
	public WebDriver driver;
	Commanmethod commanmethod =new Commanmethod();
	//Actions actions = new Actions(driver);
	//	actions.moveToElement(element);
  @BeforeTest
  public void initialSetup() {
	  String gmailid="";
	  String password="";
	  String viDeoTitle="";
	  String shortsTitle="";
	  String videotag="";
	  String shortsTag= "";
	  String HubURL= "";
	  String server="";
	  
	  
  }
  
  
  
 
  @Test()
  public void Search_with_card_detailTest() {
	  
	  try {
		  WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			driver.get("https://www.youtube.com");
			driver.manage().window().maximize();
			jsExecutor = (JavascriptExecutor) driver;
			System.out.println("Launch Youtube Successfully");
			driver.findElement(By.xpath("//input[@id='search']")).sendKeys("Best Fog Lights for Car");
			Thread.sleep(10000);
			driver.findElement(By.xpath("//button[@id='search-icon-legacy']")).click();
			Thread.sleep(5000);
			commanmethod.click(driver, "//div[@id='filter-menu']/div//a");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[contains(@title,'Sort by upload date') and @id='label']")).click();
			Thread.sleep(5000);
			System.out.println("Content is short by Date Uploaded");
			commanmethod.waitforDescriptiontobevisible(driver, jsExecutor, "//a[@title= 'Best Fog Light for Car - Ecosports Fog light Installation(Vaishnu) - Must Watch']");
			Thread.sleep(3000);
			driver.navigate().refresh();
			commanmethod.waitforpageload(driver);
			System.out.println("######   Refresh Done   ######");
			
		    commanmethod.ifpresent_thenclick(driver, jsExecutor, "//div[contains(@id,'skip-button') and @class='ytp-ad-skip-button-slot']/span");
		    System.out.println("**********   Clicked on Skip Ads Button  ********");
		    Thread.sleep(1000);
		    commanmethod.ifpresent_thenclick(driver, jsExecutor,"//div[contains(@class,'button-container')]/yt-button-renderer[@id='dismiss-button']");
		    System.out.println("***********   Clicked on Skip Trial Button  *********");
		    Thread.sleep(1000);
		    commanmethod.click(driver, "//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		   // commanmethod.ifpresent_thenclick(driver, jsExecutor,"//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		    System.out.println("********Disable  auto play Option  *******");
		    commanmethod.click(driver,"//button[contains(@title,'Mute') and contains(@class,'ytp-mute-button')]");
		    System.out.println("***** Mute the Video ******** ");
		    commanmethod.StopThread(driver, jsExecutor, "(//a[@class='ytp-ce-covering-overlay'])[2]", 2);
		    driver.quit();
		    

	

			

		
					
	} catch (Exception e) {
		System.out.print(e);
		driver.quit();
		// TODO: handle exception
	}

  }
  
@
Test  
public void Search_with_card_detailTest2() {
	  
	  try {
		  WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			driver.get("https://www.youtube.com");
			driver.manage().window().maximize();
			jsExecutor = (JavascriptExecutor) driver;
			System.out.println("Launch Youtube Successfully");
			driver.findElement(By.xpath("//input[@id='search']")).sendKeys("Best Fog Lights for Car");
			Thread.sleep(10000);
			driver.findElement(By.xpath("//button[@id='search-icon-legacy']")).click();
			Thread.sleep(5000);
			commanmethod.click(driver, "//div[@id='filter-menu']/div//a");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[contains(@title,'Sort by upload date') and @id='label']")).click();
			Thread.sleep(5000);
			System.out.println("Content is short by Date Uploaded");
			commanmethod.waitforDescriptiontobevisible(driver, jsExecutor, "//a[@title= 'Best Fog Light for Car - Ecosports Fog light Installation(Vaishnu) - Must Watch']");
			Thread.sleep(3000);
			driver.navigate().refresh();
			commanmethod.waitforpageload(driver);
			System.out.println("######   Refresh Done   ######");
			
		    commanmethod.ifpresent_thenclick(driver, jsExecutor, "//div[contains(@id,'skip-button') and @class='ytp-ad-skip-button-slot']/span");
		    System.out.println("**********   Clicked on Skip Ads Button  ********");
		    Thread.sleep(1000);
		    commanmethod.ifpresent_thenclick(driver, jsExecutor,"//div[contains(@class,'button-container')]/yt-button-renderer[@id='dismiss-button']");
		    System.out.println("***********   Clicked on Skip Trial Button  *********");
		    Thread.sleep(1000);
		    commanmethod.click(driver, "//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		   // commanmethod.ifpresent_thenclick(driver, jsExecutor,"//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		    System.out.println("********Disable  auto play Option  *******");
		    commanmethod.click(driver,"//button[contains(@title,'Mute') and contains(@class,'ytp-mute-button')]");
		    System.out.println("***** Mute the Video ******** ");
		    commanmethod.StopThread(driver, jsExecutor, "(//a[@class='ytp-ce-covering-overlay'])[2]" , 10);
		    driver.quit();
		    

	

			

		
					
	} catch (Exception e) {
		System.out.print(e);
		driver.quit();
		// TODO: handle exception
	}

  }

@Test
public void Search_with_card_detailTest3() {
	  
	  try {
		 WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
			driver.get("https://www.youtube.com");
			driver.manage().window().maximize();
			jsExecutor = (JavascriptExecutor) driver;
			System.out.println("Launch Youtube Successfully");
			driver.findElement(By.xpath("//input[@id='search']")).sendKeys("Best Fog Lights for Car");
			Thread.sleep(10000);
			driver.findElement(By.xpath("//button[@id='search-icon-legacy']")).click();
			Thread.sleep(5000);
			commanmethod.click(driver, "//div[@id='filter-menu']/div//a");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[contains(@title,'Sort by upload date') and @id='label']")).click();
			Thread.sleep(5000);
			System.out.println("Content is short by Date Uploaded");
			commanmethod.waitforDescriptiontobevisible(driver, jsExecutor, "//a[@title= 'Best Fog Light for Car - Ecosports Fog light Installation(Vaishnu) - Must Watch']");
			Thread.sleep(3000);
			driver.navigate().refresh();
			commanmethod.waitforpageload(driver);
			System.out.println("######   Refresh Done   ######");
			
		    commanmethod.ifpresent_thenclick(driver, jsExecutor, "//div[contains(@id,'skip-button') and @class='ytp-ad-skip-button-slot']/span");
		    System.out.println("**********   Clicked on Skip Ads Button  ********");
		    Thread.sleep(1000);
		    commanmethod.ifpresent_thenclick(driver, jsExecutor,"//div[contains(@class,'button-container')]/yt-button-renderer[@id='dismiss-button']");
		    System.out.println("***********   Clicked on Skip Trial Button  *********");
		    Thread.sleep(1000);
		    commanmethod.click(driver, "//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		   // commanmethod.ifpresent_thenclick(driver, jsExecutor,"//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
		    System.out.println("********Disable  auto play Option  *******");
		    commanmethod.click(driver,"//button[contains(@title,'Mute') and contains(@class,'ytp-mute-button')]");
		    System.out.println("***** Mute the Video ******** ");
		    commanmethod.StopThread(driver, jsExecutor, "(//a[@class='ytp-ce-covering-overlay'])[2]",10);
		    driver.close();
		    

	

			

		
					
	} catch (Exception e) {
		System.out.print(e);
		driver.quit();
		// TODO: handle exception
	}
	  
}


@Test
public void Search_with_card_detailTest4() {
		  
		  try {
			  WebDriverManager.edgedriver().setup();
				driver = new EdgeDriver();
				driver.get("https://www.youtube.com");
				driver.manage().window().maximize();
				jsExecutor = (JavascriptExecutor) driver;
				System.out.println("Launch Youtube Successfully");
				driver.findElement(By.xpath("//input[@id='search']")).sendKeys("Best Fog Lights for Car");
				Thread.sleep(10000);
				driver.findElement(By.xpath("//button[@id='search-icon-legacy']")).click();
				Thread.sleep(5000);
				commanmethod.click(driver, "//div[@id='filter-menu']/div//a");
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[contains(@title,'Sort by upload date') and @id='label']")).click();
				Thread.sleep(5000);
				System.out.println("Content is short by Date Uploaded");
				commanmethod.waitforDescriptiontobevisible(driver, jsExecutor, "//a[@title= 'Best Fog Light for Car - Ecosports Fog light Installation(Vaishnu) - Must Watch']");
				Thread.sleep(3000);
				driver.navigate().refresh();
				commanmethod.waitforpageload(driver);
				System.out.println("######   Refresh Done   ######");
				
			    commanmethod.ifpresent_thenclick(driver, jsExecutor, "//div[contains(@id,'skip-button') and @class='ytp-ad-skip-button-slot']/span");
			    System.out.println("**********   Clicked on Skip Ads Button  ********");
			    Thread.sleep(1000);
			    commanmethod.ifpresent_thenclick(driver, jsExecutor,"//div[contains(@class,'button-container')]/yt-button-renderer[@id='dismiss-button']");
			    System.out.println("***********   Clicked on Skip Trial Button  *********");
			    Thread.sleep(1000);
			    commanmethod.click(driver, "//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
			   // commanmethod.ifpresent_thenclick(driver, jsExecutor,"//button[contains(@title,'Autoplay is on') and @class='ytp-button']");
			    System.out.println("********Disable  auto play Option  *******");
			    commanmethod.click(driver,"//button[contains(@title,'Mute') and contains(@class,'ytp-mute-button')]");
			    System.out.println("***** Mute the Video ******** ");
			    commanmethod.StopThread(driver, jsExecutor, "(//a[@class='ytp-ce-covering-overlay'])[2]", 10);
			    driver.close();
			    

		

				

			
						
		} catch (Exception e) {
			System.out.print(e);
			driver.quit();
			// TODO: handle exception
		}

}



 
}
